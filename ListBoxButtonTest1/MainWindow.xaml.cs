﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace ListBoxButtonTest1
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = new HogeViewModel();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            var hogeRow = button.DataContext as HogeRow;
            textBlock1.Text = String.Format("clicked: id={0}, text='{1}'", hogeRow.id, hogeRow.text);
        }
    }
}
