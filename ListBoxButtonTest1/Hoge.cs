﻿using System.Collections.Generic;
using System.IO;

namespace ListBoxButtonTest1
{

    public class HogeRow
    {
        public int id { get; set; }
        public string text { get; set; }
    }

    public class HogeViewModel
    {

        private int _lastAssignedId = 0;
        public List<HogeRow> data { get; private set; }

        public HogeViewModel()
        {
            data = new List<HogeRow>();
            addData("hoge");
            addData("fuga");
            loadDataFromText(@"TextFile1.txt");
        }

        /// <summary>
        /// データを追加する
        /// </summary>
        public void addData(string s)
        {
            data.Add(new HogeRow { id = _lastAssignedId++, text = s });
        }

        /// <summary>
        /// データをテキストファイルから読み込む
        /// </summary>
        private void loadDataFromText(string filename)
        {
            foreach (var line in File.ReadAllLines(filename))
            {
                addData(line);
            }
        }
    }
}
